const logger = require('../config/logger')
const { consumer } = require('../config/kafkaClient')
const queueService = require('../service/queueService')

const LOG_ORIGIN = 'kafkaConsumer'

const handleCommit = (error, data) => {
  if (error) {
    throw new Error(error)
  }
  return data
}

consumer.on('message', async (message) => {
  logger.info('incoming message', {
    logOrigin: LOG_ORIGIN,
    topic: 'incoming message',
    data: message,
  })
  switch (message.key) {
    case 'queueInitialData': {
      const startTime = new Date()
      const { audienceUUID, waitingRoomUUID, eventUUID } = JSON.parse(message.value)
      const audience = await queueService.assignQueue(audienceUUID, waitingRoomUUID, eventUUID)

      const endTime = new Date()
      consumer.commit((error, data) => {
        try {
          handleCommit(error, data)
          logger.info(null, {
            logOrigin: LOG_ORIGIN,
            assignmentDuration: endTime - startTime,
            unit: 'ms',
            eventUUID,
            audienceUUID,
          })
        } catch (err) {
          logger.error('kafka commit error', {
            logOrigin: LOG_ORIGIN,
            error: err,
          })
        }
      })
      return audience
    }
    default: {
      return false
    }
  }
})

consumer.on('error', (error) => {
  logger.error('kafka consumer error', {
    logOrigin: LOG_ORIGIN,
    error,
  })
})

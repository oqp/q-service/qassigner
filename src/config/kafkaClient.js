require('dotenv').config()

const kafka = require('kafka-node')
const logger = require('../config/logger')

const LOG_ORIGIN = 'kafkaClient'


const client = new kafka.KafkaClient({
  kafkaHost: process.env.KAFKA_HOST,
  idleConnection: process.env.KAFKA_MAX_IDLE_TIME_MS || 300000,
  reconnectOnIdle: true,
})

const consumer = new kafka.Consumer(client, [
  {
    topic: process.env.KAFKA_ASSIGN_QUEUE_TOPIC_NAME,
    partition: 0,
  },
], {
  autoCommit: false,
  fromOffset: false,
})

client.on('ready', () => {
  logger.info(`kafkaClient is ready (${process.env.KAFKA_HOST})`, {
    logOrigin: LOG_ORIGIN,
  })
})

consumer.on('offsetOutOfRange', (error) => {
  logger.info('offsetOutofRange', error)
  const { topic } = error
  const { partition } = error
  const offset = new kafka.Offset(client)
  offset.fetchEarliestOffsets([topic], (fetchError, data) => {
    if (fetchError) {
      logger.error(fetchError, {
        logOrigin: LOG_ORIGIN,
      })
    }
    const newOffset = data[topic][partition]
    consumer.setOffset(topic, partition, newOffset)
    logger.info(`set consumer offset ${topic} | ${partition} | ${newOffset}`, {
      logOrigin: LOG_ORIGIN,
    })
  })
})
module.exports = { client, consumer }

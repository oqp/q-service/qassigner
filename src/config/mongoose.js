require('dotenv').config({ path: '/' })

const mongoose = require('mongoose')

const logger = require('./logger')


mongoose.set('bufferCommands', false)
mongoose.set('useFindAndModify', false)
mongoose.set('useNewUrlParser', true)
mongoose.set('useCreateIndex', true)
mongoose.connect(
  `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`,
  { connectTimeoutMS: 2000 },
)

const LOG_ORIGIN = 'mongoose'
const db = mongoose.connection
db.on('error', (error) => {
  logger.error({
    logOrigin: LOG_ORIGIN,
    error,
  })
})
db.once('open', () => {
  logger.info({
    logOrigin: LOG_ORIGIN,
    message: 'connected to mongodb server',
  })
})


module.exports = { db, mongoose }

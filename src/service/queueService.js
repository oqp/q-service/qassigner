const logger = require('../config/logger')
const { mongoose } = require('../config/mongoose')

const { WaitingRoomModel } = require('../model/waitingRoom')
const { AudienceModel, audienceSchema } = require('../model/audience')

const counterSchema = new mongoose.Schema({
  _id: { type: String, required: true },
  seq: { type: Number, default: 1 },
})

module.exports = {
  async getWaitingRoomList() {
    const waitingRooms = await WaitingRoomModel.find()
    return waitingRooms
  },
  async getWaitingRoom(waitingRoomUUID) {
    const waitingRoom = await WaitingRoomModel.find({ _id: waitingRoomUUID })
    return waitingRoom
  },
  async createWaitingRoom(website, subdomain) {
    const waitingRoom = new WaitingRoomModel({ website, subdomain })
    return waitingRoom.save()
  },
  async assignQueue(audienceUUID, waitingRoomUUID, eventUUID) {
    const dbConnection = mongoose.connection.useDb(`event-${eventUUID}`)

    const CounterModel = dbConnection.model('Counter', counterSchema)
    const count = await CounterModel.findByIdAndUpdate('entityId', { $inc: { seq: 1 } }, { upsert: true, new: true })
    const TempAudienceModel = dbConnection.model('Audience', audienceSchema)
    const queueNumber = count.seq
    const audience = new TempAudienceModel({
      uuid: audienceUUID,
      status: 'waiting',
      queueNumber,
      enteredQueueAt: new Date(),
    })
    const savedAudience = await audience.save()
    logger.info(null, {
      logOrigin: 'queueService',
      topic: 'assigned',
      audienceUUID: savedAudience.uuid,
      queueNumber: savedAudience.queueNumber,
    })
    return savedAudience
  },
}

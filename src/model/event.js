const mongoose = require('mongoose')

const { Schema, SchemaTypes } = mongoose

const eventSchema = new Schema({
  id: SchemaTypes.ObjectId,
  uuid: String,
  name: String,
  openQueue: Date,
  closeQueue: Date,
  maxOutflowAmount: Number,
  sessionTime: Number,
  currentQueueNumber: Number,
  redirectURL: String,
  isActive: Boolean,
  waitingRoomId: String,
})

const EventModel = mongoose.model('Event', eventSchema)

module.exports = { eventSchema, EventModel }

const { Schema, SchemaTypes } = require('mongoose')

const { mongoose } = require('../config/mongoose')


const audienceSchema = new Schema({
  id: SchemaTypes.ObjectId,
  uuid: String,
  queueNumber: { type: Number, default: 1, unique: true },
  telno: String,
  email: String,
  enteredQueueAt: Date,
  status: String,
  eligibledAt: Date,
  servedAt: Date,
  canceledAt: Date,
})

const AudienceModel = mongoose.model('Audience', audienceSchema)

module.exports = { audienceSchema, AudienceModel }

const mongoose = require('mongoose')

const { Schema, SchemaTypes } = mongoose

const waitingRoomSchema = new Schema({
  id: SchemaTypes.ObjectId,
  website: String,
  subdomain: String,
  uuid: String,
  name: String,
})

const WaitingRoomModel = mongoose.model('WaitingRoom', waitingRoomSchema)

module.exports = { waitingRoomSchema, WaitingRoomModel }

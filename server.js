require('dotenv').config()

const express = require('express')
const bodyParser = require('body-parser')
const logger = require('./src/config/logger')

const app = express()
const port = process.env.PORT || 3000

app.use(bodyParser.json({ extended: true }))

app.get('/', (req, res) => res.send('Hello World!'))

app.listen(port, () => logger.info({
  logOrigin: 'express',
  message: `QAssigner app listening on port ${port}!`,
}))

const kafkaConsumerController = require('./src/controller/kafkaConsumerController')

app.set('kafkaConsumerController', kafkaConsumerController)
